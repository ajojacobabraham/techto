# How to install Canon LBP 2900 Driver on Arch Based Distros 

$1: Use an AUR helper to install capt-src AUR package

$2: Add user to lp group
```
gpasswd -a your_user lp

```
$3: Reboot & Relogin

$4: 
``` 
/usr/bin/lpadmin -p LBP2900 -m CNCUPSLBP2900CAPTK.ppd -v ccp://localhost:59687 -E 

```

$5: Run following command to check the mountpoint for the printer automatically created.
```
ls /dev/usb/

```

$6: 
```
sudo /usr/bin/ccpdadmin -p LBP2900 -o /dev/usb/lp0

``` 

$7: Check status of printer using 

```
/usr/bin/captstatusui -P LBP2900

```


### Precaution commands:

Many a times, the printer might not work due to various reasons. 

Steps / Commands to be followed then:

1. Reinstall CUPS
2. Reinstall CAPT
3. ls -l /var/ccp
4. sudo mkdir /var/ccpd
5. sudo mkfifo /var/ccpd/fifo0
6. sudo chown -R lp:lp /var/ccpd
7. sudo chown -R user_name:lp /var/ccpd (replace username with $LOGNAME)

8. ldd /usr/bin/captfilter (produces this below output)
        linux-gate.so.1 (0xf76f7000)
 libpopt.so.0 => /usr/lib32/libpopt.so.0 (0xf76b0000)
 libc.so.6 => /usr/lib32/libc.so.6 (0xf74ef000)
 /lib/ld-linux.so.2 (0xf76f9000)

9. ldd /usr/bin/capt* | sort | uniq | grep "not found" (should give you no output )

Last enable both cups and capt for startup by the command

sudo systemctl enable org.cups.cupsd.service
sudo systemctl enable ccpd.service
